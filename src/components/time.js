import { Component } from "react";

let today = new Date();
let hour = today.getHours();
let minute = today.getMinutes();
let second = today.getSeconds();
let statusDay = hour >= 12 ? "AM" : "PM";

class Time extends Component {
    constructor(props) {
        super(props);
        this.state = { color: "" }
    }

    onBtnChangeColor = () => {
        console.log("Nút đc bấm");
        if(second % 2 === 0) {
            this.setState({
                color: "red"
            })
        } else {
            this.setState({
                color: "blue"
            })
        }
    }

    render() {
        return (
            <div>
                <h2>Hello, World!</h2>
                <h4 style={{ color: this.state.color }}>Time is: {hour}:{minute}:{second} - {statusDay}</h4>
                <button onClick={this.onBtnChangeColor}>Change color</button>
            </div>
        )
    }
}

export default Time;